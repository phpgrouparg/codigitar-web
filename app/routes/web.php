<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
  'prefix' => '{locale}',
  'where' => ['locale' => 'es|en|pt' ], //implode( '|', array_keys(config('app.available_locales')))
  'middleware' => 'setlocale'
], function() {

  Auth::routes();

  Route::get('/', 'HomeController@index')->name('home');
  Route::get('/contactar', 'HomeController@contactar')->name('contactar');
  Route::get('/procesos', 'HomeController@procesos')->name('procesos');
  Route::get('/productos', 'HomeController@productos')->name('productos');

});

Route::get('/{page?}', 'HomeController@redireccionar');