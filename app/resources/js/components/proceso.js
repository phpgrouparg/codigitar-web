import carousel from 'vue-owl-carousel'

window.pasarHoja = function pasarHoja(handler) {
	document.getElementById(handler).click();
}
console.log('en proceso.js');
const Proceso = new Vue({
	el: '#agregado',
	components: { carousel },
	methods: {
		previo: function() {
			pasarHoja(this.$children[0].prevHandler);

		},
		posterior: function() {
			pasarHoja(this.$children[0].nextHandler);
		}
	},
	mounted() {
		Array.from(this.$el.querySelectorAll('.owl-dot span')).forEach((dot, idx) => {
			dot.innerHTML =  [1,2,3,'+!',4,5,'↗'][idx];
		});
	}
});

export default Proceso;