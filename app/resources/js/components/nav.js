import smoothscroll from 'smoothscroll-polyfill';
import {isSmall, onBPchanged } from '../globals/breakpoints';
import { Slide } from 'vue-burger-menu'

smoothscroll.polyfill();

function getVisibleSection() {
  const scrolled = window.pageYOffset || document.documentElement.scrollTop;
  const height = window.innerHeight;
  return Array.from(document.documentElement.querySelectorAll('section[id]')).find((section) => {
    return (
      section.offsetTop < scrolled + height * .2 &&
      section.offsetHeight + section.offsetTop > scrolled + height * .2
    )
  });
}

const lenguajes = [{codigo: 'es', etiqueta: 'Castellano'}, {codigo:'en', etiqueta:'English'}, {codigo:'pt', etiqueta:'Portuguese'}];

const nav = new Vue({
  el: '.nav',
  data: {
    visible: !isSmall(),
    actived: null,
    langs: [],
    esteIdioma: 'es',
    idioma_expandido: false,
    montado: false,
    params: {}
  },
  delimiters: ['<%', '%>'],
  components: {
    Slide
  },
  methods: {
    toggleMenu(force) {
      this.visible = (typeof force === 'boolean') ? force : !this.visible;
    },
    isVisible(id) {
      const section = getVisibleSection();
      return (section) ? section.id === id : false;
    },
    smoothScroll(e) {
      const target = document.getElementById(e.target.href.replace(/.+#/, ''));
      if(target) {
        e.preventDefault();
        target.scrollIntoView({ behavior: 'smooth' });
        var currentUrl = new URL(e.target.href);
        history.pushState({}, e.target.innerText, currentUrl.pathname + '#' + target.id);
      }
    },
    toggleBorder(section) {
      var accion = (section === 'inicio') ? 'remove' : 'add';
      this.$el.classList[accion]('nav--border');
    },
    cambiarIdioma(destino) {
      var pathname = (location.pathname.length > 1)
        ? location.pathname.replace(new RegExp('^/' + this.esteIdioma + '([/?]|$)', ''), '/' + destino + '$1')
        : '/' + destino;
      return location.origin + pathname + location.search + location.hash;
    },
    expandColaps(evt) {
      evt.preventDefault();
      evt.stopPropagation();
      var id = evt.target.dataset.colapsable;
      this[id] = !this[id];
    },
    changeToPNG(evt) {
      var elm = evt.target;
      elm.src = elm.src.replace(/svg$/i, 'png');
    }
  },
  mounted: function() {
    this.montado = true;
    var tieneIdioma = location.pathname.match(/^\/(\w{2})([/?]|$)/);
    var self = this;
    self.esteIdioma = tieneIdioma ? tieneIdioma[1] : 'es';
    self.langs = lenguajes.filter(function(elm) {
      return elm.codigo !== self.esteIdioma;
    });
    this.$children[0].$el.classList.remove('oculto');
    this.params = JSON.parse(this.$refs.params.value);
    this.$el.querySelectorAll('.nav__menu__bandera').att
  }
});

onBPchanged(function() {
  if(!isSmall() && !nav.visible) {
    nav.toggleMenu(true);
  }
});
document.addEventListener('DOMContentLoaded', function() {

  let last_scroll = 0;
  let ticking = false;
  window.addEventListener('scroll', function(e) {

    if (!ticking && Math.abs(last_scroll - window.scrollY) > 10) {
      last_scroll = window.scrollY;

      window.requestAnimationFrame(function() {
        const current = getVisibleSection();
        if(current) {
          nav.toggleBorder(current.id);
          nav.actived = current.id;
        }
        ticking = false;
      });

      ticking = true;
    }

  });
}, false);



export default nav