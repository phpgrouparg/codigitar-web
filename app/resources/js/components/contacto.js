import axios from 'axios';
import validator from 'email-validator';
import Spinner from 'vue-simple-spinner'

const Enviando = {
	template: '#contacto-enviando',
	components: {
    Spinner
	}
};

const Enviado = {
	template: '#contacto-enviado'
};

const ContactoError = {
	template: '#contacto-error',
	methods: {
		reintentar() {
			this.$parent.reintentar();
		}
	}
}

const validaciones = {
	nombre: function(val) {
		return (!val) ? 0 : ((val.length > 3) ? 2 : 1);
	},
	correoe: function(val) {
		return (!val) ? 0 : (validator.validate(val) ? 2 : 1);
	},
	codarea: function(val) {
		return (!val) ? 0 : (/\+?\d{2,6}/.test(val) ? 2 : 1);
	},
	telefono: function(val) {
		return (!val) ? 0 : (/\+?\d{5,16}/.test(val) ? 2 : 1);
	},
  descripcion: function(val) {
    return (!val) ? 0 : (/(\w{1,20}\s)/g.match(val).length > 3 ? 2 : 1);
  }
}

const contactForm = new Vue({
	el: '#contacto',
	components: {
		'contacto-enviado': Enviado,
		'contacto-enviando': Enviando,
		'contacto-error': ContactoError
	},
	data() {
		return {
			nombre: '',
			correoe: '',
			codarea: '',
			telefono: '',
      productos: [],
      descripcion: '',
			enviado: false,
			enviando: false,
			error: false,
			esLargo: false,
			validos: {
				nombre: 0, // 0: vacío, 1: inválido, 2: válido
				correoe: 0,
				codarea: 0,
				telefono: 0
			}
		}
	},
	methods: {
		enviar() {
			if(!this.validar()) {
				return false;
			}
			this.enviando = true;
			axios.post('/contacto-minimo', {
				nombre: this.nombre,
				correoe: this.correoe,
				codarea: this.codarea,
				telefono: this.telefono,
        productos: this.productos,
        descripcion: this.descripcion,
        productos: this.productos
			}).then(() => {
				this.enviado = true;
				this.error = false;
			}).catch((err) => {
				this.enviado = false;
				this.error = true;
			}).finally(() => {
				this.enviando = false;
			})
		},
		reintentar() {
			this.error = false;
		},
		validar() {
			this.validos.nombre = validaciones.nombre(this.nombre);
			this.validos.correoe = validaciones.correoe(this.correoe);
			let todosBien = 4;
			if(this.esLargo) {
				this.validos.codarea = validaciones.codarea(this.codarea);
				this.validos.telefono = validaciones.telefono(this.telefono);
				todosBien = 8;
			}

			return (Object.keys(this.validos).reduce((prev, campo) => prev + this.validos[campo], 0) === todosBien);
		}
	},
	mounted: function() {
		if(this.$el.dataset && this.$el.dataset.forma === 'largo') {
			this.esLargo = true;
		}
	}
});

export default contactForm;