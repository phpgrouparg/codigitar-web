const breakpoints = {
  sm: 640,
  md: 1024,
  lg: 1200,
  xl: Infinity
}


const isSmall = function() {
	return window.matchMedia('(max-width: ' + breakpoints.sm + 'px').matches;
}

const currentBP = function() {
	const currWidth = window.innerWidth;
	return Object.entries(breakpoints).reduce((prev, entry) => {
		if(typeof prev === 'object') {
			return prev;
		}
		if(currWidth <= entry[1] && currWidth > prev) {
			return {name: entry[0], min: prev, max: entry[1]};
		}
		return entry[1];
	}, 0);
}

function onBPchanged(fn) {
	if(!window.resizeBP) {
		window.currentBPname = currentBP().name;
		window.addEventListener('resize', function() {
			const bp = currentBP()
			if(bp.name != window.currentBPname) {
				const event = new CustomEvent('breakpoint-changed', { detail: bp });
				window.dispatchEvent(event);
				window.currentBPname = bp;
			}
		});
		window.resizeBP = true;
	}
	window.addEventListener('breakpoint-changed', fn);
}

export { breakpoints, isSmall, currentBP, onBPchanged };