<?php
return [
	'algo_podemos' => 'Something we can do',
	'productos_servicios' => 'PRODUCTS / SERVICES',
	'sistemas_gestion' => 'Data and content management systems',
	'apps' => 'Applications for mobile devices',
	'ecommerce' => "E-commercev tools",
	'servicios_web' => 'Web services',
	'infra' => 'Release, migration and control of cloud infrastructure',
	'diseno' => 'Design, brand y marketing for digital environment',
	'conocer_mas' => 'Find out more',
	'i_d' => 'R + D',
	'monorepo' => 'One development, multiple devices',
	'inteligencia_artificial' => 'Artificial Intelligence',
	'domotica' => 'Domotics',
	'internet_cosas' => 'Internet of things',
	'robotica' => 'Robotics',
	'realidad_aumentada' => 'Augmented Reality',
	'cuentame_mas' => 'Tell me more',
	'somos_codigitadores' => 'We are CODIGITERS'
];