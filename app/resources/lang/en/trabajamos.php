<?php
return [
	'somos_compania' => 'We are a company dedicated and specialized in the development of software for companies',
	'junto_clientes' => 'Together with our clients, we build the business information base',
	'automatizamos_optimizamos' => 'We automate and optimize manual processes, and thus reduce operating costs',
	'comenzamos_juntos' => 'Do we start a project together?',
	'me_interesa' => "I'm interested"
];