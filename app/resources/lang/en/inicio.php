<?php
return [
	'buscamos_digitalizacion' => 'We seek the digitalization of companies, applying and adapting technology to the needs of our customers',
	'cuentame_mas' => 'Tell me more',
	'asi_trabajamos' => 'At Codigitar we work like this'
];