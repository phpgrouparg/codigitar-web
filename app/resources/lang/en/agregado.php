<?php
return [
	'nuestro_agregado' => 'OUR ADDED VALUE!',
	'forma_progresiva' => 'Progressively and with short deadlines, we activate system functionalities. ' .
		'The customer, partially and increasingly, makes use of the product, ' .
		'without waiting at the end of the project, being able to nurture the development.',
	'conozca_proceso' => 'Get to know our process',
	'ellos_cuentan' => 'They already have their processes'
];