<?php
return [
	'entonces' => 'So ... Do we get in contact?',
	'error_nombre' => 'You must enter a name of at least 3 characters',
	'deja_nombre' => 'Leave us your name',
	'error_correo' => 'You must enter a valid email address',
	'deja_correo' => 'Leave us your email',
	'contactame' => 'Contact me',
	'te_contactaremos' => 'We already have your data. We will contact you shortly!',
	'error_envio' => 'There was an error in sending your data, please try again later. '.
		'Or try sending us an email to',
	'reintentar' => 'Retry',
	'entendernos_formulario' => 'To understand each other better, please complete the fields of the following form so we get in touch',
	'acordemos_cafe' => "Let's make an appointment to have a coffee",
	'error_area' => "You must enter a correct area code, you can include '+' and digits",
	'error_telefono' => 'You must enter a valid phone with only digits',
	'telefono' => 'Phone',
	'cod_area' => 'Area Code',
  'elegir_productos' => 'Choose the products of your interest',
  'deja_descripcion' => 'Describe your requirements and ideas, to allow us to help better',
	'enviar' => 'Send'
];