<?php
return [
  'inicio' => 'Home',
  'trabajamos' => 'How do we work?',
  'agregado' => 'Codigit Process',
  'podemos' => 'What we do',
  'codigitadores' => 'Codigiters',
  'contacto' => 'Contact',
  'idioma' => 'Language'
];