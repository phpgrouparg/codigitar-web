<?php
return [
	'planificacion' => 'PLANNING',
	'realiza_relevamiento' => 'A situation survey is performed. The Client and work team poses problems and objectives for process optimization',
	'estimacion' => 'ESTIMATION OF TIME COSTS',
	'nuestro_equipo' => 'Our work team proposes an estimation of development times and system estimation costs',
	'desarrollo' => 'DEVELOPMENT',
	'codigitar_lleva' => 'Codigitar carries out the proposal accepted by the client, according to the stipulated times',
	'nuestro_valor' => 'OUR ADDED VALUE',
	'forma_progresiva' => 'Progressively and with short deadlines, we activate system functionalities. '.
		'The client partially uses the product, without waiting at the end of the project, being able to nurture the development.',
	'implementacion' => 'DEPLOYMENT',
	'implementamos_producto' => 'We implement the proposed product or system by performing tests with the work team',
	'capacitacion' => 'TRAINING OF THE WORK TEAM',
	'adaptamos_tecnologia' => 'We adapt the technology to the processes that your company performs, '.
		'looking for its digitalization for the optimization of results and cost reduction',
	'conoces_proceso' => 'You know our agile work process.',
	'te_contamos' => 'We tell you about our',
	'valor_agregado' => 'ADDED VALUE',
	'emprendamos_juntos' => "Let's start a project together!",
	'de_acuerdo' => 'I agree',
	'ellos_cuentan' => 'They already have their processes'
];