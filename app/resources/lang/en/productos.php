<?php
return [
  'gestion_gimnasios' => 'Gym management',
  'enfocado_retencion' => 'Increase the control of fitness centers. Ease the maintenance, coordination and charges. ' .
    'It os highlighted by being focused on the <strong>clients retention</strong>',
  'encuestas' => 'Face-to-face surveys',
  'soporte_encuestas' => 'Supporting system for the carring out of face-to-face surveys, with the capacity of ' .
    '<strong>geolocationn</strong> and nesting answers',
  'gestion_escuela' => 'Private Schools management',
  'soporte_cobro' => 'Focused to ease the administration with the flexibility and sensibility required by the area',
  'gestion_inventarios' => 'Stock Management',
  'soporte_cobro' => 'Focus on ease the customer service in a context of <strong>price changing</strong> and regulation evolution. ' .
    'It is a scalable system, that has different modules like </i>billing, devolution y role handling</i> among others',
  'gestion_residuos' => 'Waste traceability',
  'soporte_trazabilidad' => 'Complete and flexible system for private and public institutions. ' .
    'Focused on ease the deposition and recording of residues',
  'ordenes_trabajo' => 'Work Orders Management',
  'soporte_rastreo' => 'Track and report the state an evolution of issues and requirements. With the capability of supervision and ' .
    'transfer of responsibility'
];