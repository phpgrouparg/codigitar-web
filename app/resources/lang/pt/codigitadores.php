<?php
return [
	'fundador' => 'Fundador',
	'servicios' => 'Serviços',
	'analisis' => 'Análise',
	'interfaces' => 'Interfaces',
  'jefe_comercial' => 'Chefe Comercial',
  'jefe_operativo' => 'Chefe Operacional',
  'jefe_proyectos' => 'Chefe de Projetos',
	'ampliar_info' => 'Expandir Info'
];