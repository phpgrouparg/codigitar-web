<?php
return [
	'somos_compania' => 'Somos uma empresa dedicada e especializada no desenvolvimento de software de negócios',
	'junto_clientes' => 'Junto com nossos clientes, nós construímos a base de computação empresarial',
	'automatizamos_optimizamos' => 'Automatizar e optimizar processos manuais, e, assim, reduzir os custos operacionais',
	'comenzamos_juntos' => 'Vamos começar um projeto juntos?',
	'me_interesa' => 'Interessa-me'
];