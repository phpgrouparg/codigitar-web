<?php
return [
  'gestion_gimnasios' => 'Gestão de ginásios',
  'enfocado_retencion' => 'Aumenta o controlo nas instituições desportivas. Facilita a manutenção, coordenação e cobranças. ' .
    'Destaca-se por estar focada na retenção de clientes.</strong>',
  'encuestas' => 'Pesquisas presenciais',
  'soporte_encuestas' => 'Sistema de apoio para a execução de levantamentos presenciais, com capacidade para ' .
    '<strong>Geolocalização</strong> e nidificação de resposta',
  'gestion_escuela' => 'Gestão de escolas particulares',
  'soporte_cobro' => 'Focada em facilitar a gestão com a flexibilidade e sensibilidade exigidas pela indústria',
  'gestion_inventarios' => 'Gestão de Estoques',
  'soporte_cobro' => 'Focado em facilitar o atendimento ao cliente em um ambiente de <strong>mudanças de preços</strong> e regulamentos. ' .
    'É um sistema expansível, que possui módulos para faturamento, devoluções e gestão de papéis, entre outros',
  'gestion_residuos' => 'Rastreabilidade dos resíduos',
  'soporte_trazabilidad' => 'Sistema completo e flexível para instituições privadas e públicas. ' .
    'Focado em facilitar a deposição e o registro de resíduos',
  'ordenes_trabajo' => 'gestão de Ordens de trabalho',
  'soporte_rastreo' => 'Acompanhe e informe o estado e a evolução das questões e exigências. Com a capacidade de supervisão e ' .
    'transferência de responsabilidade'
];