<?php
return [
	'planificacion' => 'PLANEJAMENTO',
	'realiza_relevamiento' => 'Um levantamento de situação é realizado. Cliente ea equipe coloca problemas e objetivos para otimização de processos',
	'estimacion' => 'ESTIMATIVA DOS CUSTOS E TEMPO',
	'nuestro_equipo' => 'Nossa equipe apresenta uma estimativa do tempo e custos de desenvolvimento da sistema',
	'desarrollo' => 'DESENVOLVIMENTO',
	'codigitar_lleva' => 'Codigitar realiza a proposta aceita pelo cliente, de acordo com os prazos estipulados',
	'nuestro_valor' => 'O NOSSO VALOR ACRESCENTADO',
	'forma_progresiva' => 'Com prazos progressivos e curtas forma, ativa-mos funções do sistema. '.
		'O cliente parcialmente a utilização do produto, sem esperar o fim do projeto, e pode alimentar o desenvolvimento.',
	'implementacion' => 'IMPLEMENTAÇÃO',
	'implementamos_producto' => 'Implementamos o produto ou sistema proposto, realizando testes com a equipe de trabalho',
	'capacitacion' => 'FORMAÇÃO DA EQUIPE DE TRABALHO',
	'adaptamos_tecnologia' => 'Adaptamos a tecnologia aos processos que sua empresa realiza, buscando digitalização '.
		'para otimização de resultados e redução de custos',
	'conoces_proceso' => 'Você sabe o nosso trabalho processo ágil.',
	'te_contamos' => 'Nós dizemos sobre nossa',
	'valor_agregado' => 'VALOR ACRESCENTADO',
	'emprendamos_juntos' => 'Vamos começar um projeto juntos!',
	'de_acuerdo' => 'Estou de acordo',
	'ellos_cuentan' => 'Eles já têm os seus processos'
];