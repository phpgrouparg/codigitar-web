<?php
return [
  'inicio' => 'Inicial',
  'trabajamos' => 'Como trabalhamos?',
  'agregado' => 'Processos Codigitar',
  'podemos' => 'O que fazemos',
  'codigitadores' => 'Codigitadores',
  'contacto' => 'Contato',
  'idioma' => 'Idioma'
];