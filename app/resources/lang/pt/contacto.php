<?php
return [
	'entonces' => 'Então ... Nós contactado?',
	'error_nombre' => 'Você deve especificar um nome de pelo menos 3 caracteres',
	'deja_nombre' => 'Deixe o seu nome',
	'error_correo' => 'Você deve escrever um email válido',
	'deja_correo' => 'Dê-nos o seu email',
	'contactame' => 'Contata-me',
	'te_contactaremos' => 'Nós já temos os seus dados Você iria entrar em contato com você em breve!',
	'error_envio' => 'Houve um erro no envio de seus dados, por favor, tente novamente mais tarde. Ou tentar enviar um email para',
	'reintentar' => 'Repetir',
	'entendernos_formulario' => 'Para entender melhor, por favor preencha os campos abaixo e contato que',
	'acordemos_cafe' => 'Concordamos uma nomeação para um café',
	'error_area' => "Você deve digitar um código de área correto, você pode incluir '+' e dígitos",
	'error_telefono' => 'Você deve digitar um telefone válido com um dígito',
	'telefono' => 'Telefone',
	'cod_area' => 'Cod. Área',
  'elegir_productos' => 'Escolha os produtos do seu interesse',
  'deja_descripcion' => 'Descreva as suas necessidades e ideias para que possamos ajudar melhor',
	'enviar' => 'Enviar'
];