<?php
return [
	'algo_podemos' => 'Algumas das coisas que podemos fazer',
	'productos_servicios' => 'PRODUTOS / SERVIÇOS',
	'sistemas_gestion' => 'Sistemas de gestão de dados e conteúdo',
	'apps' => 'Aplicações para dispositivos móveis',
	'ecommerce' => "Ferramentas 'E-commerce'",
	'servicios_web' => 'Serviços web',
	'infra' => 'Registro, migração e controle da infraestrutura em nuvem',
	'diseno' => 'Desenho, marca e campanhas digitais',
	'conocer_mas' => 'Saiba mais',
	'i_d' => 'I + D',
	'monorepo' => 'Um desenvolvimento, vários dispositivos',
	'inteligencia_artificial' => 'Inteligência Artificial',
	'domotica' => 'Domótica',
	'internet_cosas' => 'Internet das coisas',
	'robotica' => 'Robótica',
	'realidad_aumentada' => 'Realidade aumentada',
	'cuentame_mas' => 'Conte-me mais',
	'somos_codigitadores' => 'Estamos CODIGITADORES'
];