<?php
return [
	'nuestro_agregado' => 'NOSSO VALOR ADICIONADO!',
	'forma_progresiva' => 'Progressivamente e com prazos curtos, ativamos as funcionalidades do sistema.' .
		'O cliente, parcial e cada vez mais, faz uso do produto,' .
		'sem esperar no final do projeto, podendo nutrir o desenvolvimento.',
	'conozca_proceso' => 'Conheça nosso processo',
	'ellos_cuentan' => 'Eles já têm seus processos'
];