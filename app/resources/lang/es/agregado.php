<?php
return [
	'nuestro_agregado' => '¡NUESTRO VALOR AGREGADO!',
	'forma_progresiva' => 'De forma progresiva y con plazos cortos, activamos funcionalidades del sistema.' .
		'El cliente, de manera parcial y creciente, hace uso del producto,' .
		'sin esperar al final del proyecto, pudiendo nutrir el desarrollo.',
	'conozca_proceso' => 'Conozca nuestro proceso',
	'ellos_cuentan' => 'Ellos ya cuentan con sus procesos'
];