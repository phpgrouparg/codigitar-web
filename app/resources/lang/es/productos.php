<?php
return [
  'gestion_gimnasios' => 'Gestión de gimnasios',
  'enfocado_retencion' => 'Incrementa el control en instituciones deportivas. Facilita el mantenimiento, la coordinación y los cobros. ' .
    'Se destaca por estar enfocado en la <strong>retención de clientes</strong>',
  'encuestas' => 'Encuestas presenciales',
  'soporte_encuestas' => 'Sistema de soporte para la ejecución de encuestas presenciales, con capacidad de ' .
    '<strong>geolocalización</strong> y anidación de respuestas',
  'gestion_escuela' => 'Gestión de Colegios privados',
  'soporte_cobro' => 'Enfocado en facilitar la gestión con la flexibilidad y sensibilidad que exige el rubro',
  'gestion_inventarios' => 'Gestión de Inventarios',
  'soporte_cobro' => 'Enfocado en facilitar la atención al cliente en un ambiente de <strong>cambios de precios</strong> y reglamentaciones. ' .
    'Es un sistema ampliable, que cuenta con módulos de </i>facturación, devolución y administración de roles</i> entre otros',
  'gestion_residuos' => 'Trazabilidad de residuos',
  'soporte_trazabilidad' => 'Completo y flexible sistema para instituciones privadas y públicas. ' .
    'Enfocado en facilitar la deposición y registro de los residuos',
  'ordenes_trabajo' => 'Gestión de Órdenes de Trabajo',
  'soporte_rastreo' => 'Rastreo y reporte del estado y evolución de incidentes y requerimientos. Con la capacidad de supervisión y '.
    'transferencia de responsabilidades'
];