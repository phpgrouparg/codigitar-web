<?php
return [
	'planificacion' => 'PLANIFICACIÓN',
	'realiza_relevamiento' => 'Se realiza un relevamiento de situación. El Cliente y equipo de trabajo plantea problemática y objetivos para optimización de procesos',
	'estimacion' => 'ESTIMACIÓN DE COSTOS DE TIEMPO',
	'nuestro_equipo' => 'Nuestro equipo de trabajo plantea una estimación de tiempos de desarrollo y costos de estimación del sistema',
	'desarrollo' => 'DESARROLLO',
	'codigitar_lleva' => 'Codigitar lleva a cabo la propuesta aceptada por el cliente, de acuerdo a los tiempos estipulados',
	'nuestro_valor' => 'NUESTRO VALOR AGREGADO',
	'forma_progresiva' => 'De forma progresiva y con plazos cortos, activamos funcionalidades del sistema. '.
		'El cliente, de manera parcial va haciendo uso del producto, sin esperar al final del proyecto, pudiendo nutrir al desarrollo.',
	'implementacion' => 'IMPLEMENTACIÓN',
	'implementamos_producto' => 'Implementamos el producto o sistema propuesto realizando pruebas con el equipo de trabajo',
	'capacitacion' => 'CAPACITACIÓN DEL EQUIPO DE TRABAJO',
	'adaptamos_tecnologia' => 'Adaptamos la tecnología a los procesos que realiza tu empresa, buscando su digitalización '.
		'para la optimización de resultados y reducción de costos',
	'conoces_proceso' => 'Conoces nuestro proceso de trabajo ágil.',
	'te_contamos' => 'Te contamos acerca de nuestro',
	'valor_agregado' => 'VALOR AGREGADO',
	'emprendamos_juntos' => '¡Emprendamos un proyecto juntos!',
	'de_acuerdo' => 'Estoy de acuerdo',
	'ellos_cuentan' => 'Ellos ya cuentan con sus procesos'
];