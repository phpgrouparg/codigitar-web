<?php
return [
	'entonces' => 'Entonces... ¿Nos contactamos?',
	'error_nombre' => 'Debes indicar un nombre de al menos 3 caracteres',
	'deja_nombre' => 'Déjanos tu nombre',
	'error_correo' => 'Debes escribir una dirección de correo electrónico válida',
	'deja_correo' => 'Déjanos tu correo-e',
	'contactame' => 'Contáctame',
	'te_contactaremos' => 'Ya tenemos tus datos ¡Te contactaremos a la brevedad!',
	'error_envio' => 'Hubo un error en el envío de tus datos, por favor intenta de nuevo más tarde. '.
		'O intenta enviarnos un correo electrónico a',
	'reintentar' => 'Reintentar',
	'entendernos_formulario' => 'Para entendernos mejor, por favor completá los campos del siguiente formulario así nos ponemos en contacto',
	'acordemos_cafe' => 'Acordemos cita para tomar un café',
	'error_area' => "Debes escribir un código de area correcto, puede incluir '+' y dígitos",
	'error_telefono' => 'Debes escribir un teléfono válido con solo dígitos',
	'telefono' => 'Teléfono',
	'cod_area' => 'Cód. Area',
  'elegir_productos' => 'Elegí los productos de tu interés',
  'deja_descripcion' => 'Describí tus necesidades e ideas para que podamos ayudar mejor',
	'enviar' => 'Enviar'
];