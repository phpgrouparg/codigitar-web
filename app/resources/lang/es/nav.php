<?php
return [
  'inicio' => 'Inicio',
  'trabajamos' => '¿Como trabajamos?',
  'agregado' => 'Procesos Codigitar',
  'podemos' => 'Lo que hacemos',
  'codigitadores' => 'Codigitadores',
  'contacto' => 'Contacto',
  'idioma' => 'Idioma'
];