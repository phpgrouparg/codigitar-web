<?php
return [
	'somos_compania' => 'Somos una compañía dedicada y especializada en el desarrollo de software para empresas',
	'junto_clientes' => 'Junto a nuestros clientes, construimos la base informática de los negocios',
	'automatizamos_optimizamos' => 'Automatizamos y optimizamos los procesos manuales, y así reducimos costos operativos',
	'comenzamos_juntos' => '¿Comenzamos un proyecto juntos?',
	'me_interesa' => 'Me Interesa'
];