<?php
return [
	'fundador' => 'Fundador',
	'servicios' => 'Servicios',
	'analisis' => 'Análisis',
	'interfaces' => 'Interfaces',
  'jefe_comercial' => 'Jefe Comercial',
  'jefe_operativo' => 'Jefe Operativo',
  'jefe_proyectos' => 'Jefe de Proyectos',
	'ampliar_info' => 'Ampliar Info'
];