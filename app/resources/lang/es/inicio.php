<?php
return [
	'buscamos_digitalizacion' => 'Buscamos la digitalización de las empresas con tecnología aplicada y adaptada a las necesidades de nuestros clientes',
	'cuentame_mas' => 'Cuéntame más',
	'asi_trabajamos' => 'Así trabajamos en Codigitar'
];