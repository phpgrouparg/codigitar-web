<?php
return [
	'algo_podemos' => 'Algunas de las cosas que podemos hacer',
	'productos_servicios' => 'PRODUCTOS / SERVICIOS',
	'sistemas_gestion' => 'Sistemas de gestión de datos y contenidos',
	'apps' => 'Aplicaciones para dispositivos móviles',
	'ecommerce' => "Herramientas 'E-commerce'",
	'servicios_web' => 'Servicios web',
	'infra' => 'Registro, migración y control de infraestructura en la nube',
	'diseno' => 'Diseño, marca y campañas digitales',
	'conocer_mas' => 'Conozca más',
	'i_d' => 'I + D',
	'monorepo' => 'Un desarrollo, múltiples dispositivos',
	'inteligencia_artificial' => 'Inteligencia Artificial',
	'domotica' => 'Domótica',
	'internet_cosas' => 'Internet de las cosas',
	'robotica' => 'Robótica',
	'realidad_aumentada' => 'Realida aumentada',
	'cuentame_mas' => 'Cuéntame más',
	'somos_codigitadores' => 'Somos CODIGITADORES'
];