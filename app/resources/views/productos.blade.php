@extends('layouts.codigitar')

@section('content')

  @include('components/nav')
  @include('components/productos-lista')
  @include('components/contacto-largo', [ 'productos' => $productos ])
  @include('components/pie')

@endsection