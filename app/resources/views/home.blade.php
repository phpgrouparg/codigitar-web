@extends('layouts.codigitar')

@section('content')

  @include('components/nav')
  @include('components/inicio')
  @include('components/trabajamos')
  @include('components/agregado')
  @include('components/clientes')
  @include('components/podemos')
  @include('components/codigitadores')
  @include('components/contacto')
  @include('components/pie')

@endsection
