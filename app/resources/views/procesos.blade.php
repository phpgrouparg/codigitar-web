@extends('layouts.codigitar')

@section('content')
  @include('components/nav')
  @include('components/proceso')
  @include('components/clientes')
  @include('components/pie')

@endsection
