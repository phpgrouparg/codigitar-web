<section class="seccion codigitadores grid-container" id="codigitadores">
	<div class="grid-x grid-margin-x grid-margin-y codigitadores__grid">
		<div class="cell small-6 small-offset-3 tablet-4 tablet-offset-0 app__centrado">
{{-- 			<div class="cell small-5 tablet-12">
				<img class="codigitadores__img" src="{{ asset('media/marco_omar.png') }}" >
			</div>
			<div class="cell small-7 tablet-12">--}}
				<div class="codigitadores__nombre">Omar Mrad</div>
				<div class="codigitadores__rol">{{__('codigitadores.fundador')}} - {{__('codigitadores.jefe_comercial' )}}</div>
				<hr class="codigitadores__hr show-for-medium">
				<img
					class="codigitadores__contactos show-for-medium" src="{{ asset('media/correo-omar.PNG') }}"
					style="min-width:{{ $imgs['omar_correo'][0]}}px;"
				>
				<a href="https://www.linkedin.com/in/omarmrad" class="boton boton--vacio codigitadores__boton">{{__('codigitadores.ampliar_info')}}</a>
			{{-- </div> --}}
		</div>
		<div class="cell small-6 small-offset-3 tablet-4 tablet-offset-0 app__centrado">
			<div class="codigitadores__nombre">Nestor Estrada</div>
			<div class="codigitadores__rol">{{__('codigitadores.fundador')}} - {{ __('codigitadores.jefe_operativo')}}</div>
			<hr class="codigitadores__hr show-for-medium">
			<img
				src="{{ asset('media/correo-nestor.PNG') }}" class="codigitadores__contactos show-for-medium"
				style="min-width:{{ $imgs['nestor_correo'][0]}}px;"
			>
			<a href="https://www.linkedin.com/in/ingenieronestor-estrada30" class="boton boton--vacio codigitadores__boton">{{__('codigitadores.ampliar_info')}}</a>
		</div>
		<div class="cell small-6 small-offset-3 tablet-4 tablet-offset-0 app__centrado">
			<div class="codigitadores__nombre">Patricio Cardó</div>
			<div class="codigitadores__rol">{{__('codigitadores.fundador')}} - {{ __('codigitadores.jefe_proyectos')}}</div>
			<hr class="codigitadores__hr show-for-medium">
			<img
				class="codigitadores__contactos show-for-medium" src="{{ asset('media/correo-patricio.PNG') }}"
				style="min-width:{{ $imgs['patricio_correo'][0]}}px;"
			>
			<a href="https://www.linkedin.com/in/patricio-cardo" class="boton boton--vacio codigitadores__boton">{{__('codigitadores.ampliar_info')}}</a>
		</div>
	</div>
</section>