<script type="text/x-template" id="contacto-enviado">
	<div class="contacto__area">
		<div class="contacto__enviado app__centrado">
			<h2>{{__('contacto.te_contactaremos')}}</h2>
		</div>
	</div>
</script>
<script type="text/x-template" id="contacto-enviando">
	<div class="contacto__area">
		<div class="contacto__enviando app__centrado">
			<vue-simple-spinner></vue-simple-spinner>
		</div>
	</div>
</script>

<script type="text/x-template" id="contacto-error">
	<div class="grid-x grid-margin-x contacto__area">
		<div class="cell small-10 small-offset-1 medium-5 medium-offset-2 contacto__col">
			{{__('contacto.error_envio')}} <img src="{{ asset('/media/correo-team-blanco.png') }}">
		</div>
		<div class="cell small-10 small-offset-1 medium-3 contacto__col">
			<button class="boton boton--revert" @click="reintentar">{{__('contacto.reintentar')}}</button>
		</div>
	</div>
</script>