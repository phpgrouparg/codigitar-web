@section('productos_lista')
	<li>{{__('podemos.sistemas_gestion')}}</li>
	<li>{{__('podemos.apps')}}</li>
	<li>{{__('podemos.ecommerce')}}</li>
	<li>{{__('podemos.servicios_web')}}</li>
	<li>{{__('podemos.infra')}}</li>
	<li>{{__('podemos.diseno')}}</li>
@endsection
@section('i_d_lista')
	<li>{{ __('podemos.monorepo') }}</li>
	<li>{{ __('podemos.inteligencia_artificial')}}</li>
	<li>{{ __('podemos.domotica') }}</li>
	<li>{{ __('podemos.internet_cosas')}}</li>
	<li>{{ __('podemos.robotica')}}</li>
	<li>{{ __('podemos.realidad_aumentada')}}</li>
@endsection
<section class="seccion seccion--revert seccion--fit podemos" id="podemos">
	<div class="grid-x proximo proximo--blanco proximo--arriba">
		<div class="cell small-10 small-offset-1 medium-6 medium-offset-3">
			<button class="proximo__encabezado" data-hacia="podemos">
				{{ __('podemos.algo_podemos')}}
			</button>
		</div>
	</div>
	<div class="grid-x grid-margin-x podemos__area show-for-small-only">
		<div class="cell small-12 medium-5 podemos__col">
			<h3 class="podemos__area__subtitulo">{{__('podemos.productos_servicios')}}</h3>
			<ul class="podemos__area__lista">
				@yield('productos_lista')
			</ul>
			<div class="app__centrado podemos__area__wbot">
				<a href="{{ route('productos', app()->getLocale() ) }}" class="boton boton--revert podemos__area__boton">{{ __('podemos.conocer_mas')}}</a>
			</div>
		</div>
		<div class="cell medium-1 show-for-medium">&nbsp;</div>
		<div class="cell small-12 medium-5 podemos__col">
			<h3 class="podemos__area__subtitulo">{{ __('podemos.i_d')}}</h3>
			<ul class="podemos__area__lista">
				@yield('i_d_lista')
			</ul>
			<div class="app__centrado podemos__area__wbot">
				<a href="" class="boton boton--revert podemos__area__boton">{{ __('podemos.cuentame_mas')}}</a>
			</div>
		</div>
	</div>
	<div class="show-for-medium podemos__area">
		<div class="grid-x grid-margin-x podemos__fila">
			<div class="cell medium-5 podemos__col">
				<h3 class="podemos__area__subtitulo">{{ __('podemos.productos_servicios')}}</h3>
			</div>
			<div class="cell medium-1 show-for-medium">&nbsp;</div>
			<div class="cell medium-5 podemos__col">
				<h3 class="podemos__area__subtitulo">{{ __('podemos.i_d')}}</h3>
			</div>
		</div>
		<div class="grid-x grid-margin-x podemos__fila">
			<div class="cell medium-5 podemos__col">
				<ul class="podemos__area__lista">
					@yield('productos_lista')
				</ul>
			</div>
			<div class="cell medium-1 show-for-medium">&nbsp;</div>
			<div class="cell medium-5 podemos__col">
				<ul class="podemos__area__lista">
					@yield('i_d_lista')
				</ul>
			</div>
		</div>
		<div class="grid-x grid-margin-x podemos__fila">
			<div class="cell small-12 medium-5 podemos__col">
				<div class="app__centrado podemos__area__wbot">
					<a href="{{ route('productos', app()->getLocale()) }}" class="boton boton--revert podemos__area__boton">{{ __('podemos.conocer_mas')}}</a>
				</div>
			</div>
			<div class="cell medium-1 show-for-medium">&nbsp;</div>
			<div class="cell small-12 medium-5 podemos__col">
				<div class="app__centrado podemos__area__wbot">
					<a href="" class="boton boton--revert podemos__area__boton">{{ __('podemos.cuentame_mas')}}</a>
				</div>
			</div>
		</div>
	</div>



		</div>
	</div>

	<div class="grid-x podemos__tecno">
		<div class="cell small-12 medium-11 podemos__tecno__cell">
			@foreach($tecnos as $tecno)
				<div class="tooltip podemos__tecno__elem">
					<img src="{{ asset('/media/tecnologias/' . $tecno . '.svg')}}"
						onerror="this.src='{{ asset('/media/tecnologias/' . $tecno . '.png')}}'"
						class="podemos__tecno__cell__img"
						alt="{{ ucfirst($tecno) }}"
					>
					<div class="tooltiptext">{{ ucfirst($tecno) }}</div>
				</div>


			@endforeach
		</div>
	</div>
	<div class="grid-x proximo">
		<div class="cell small-10 small-offset-1 medium-6 medium-offset-3">
			<button class="proximo__encabezado" data-hacia="agregado">
				{{__('podemos.somos_codigitadores')}}
			</button>
		</div>
	</div>
</section>