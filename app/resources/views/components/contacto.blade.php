@include('components.contacto-partes')

<section class="seccion seccion--revert seccion--fit contacto" id="contacto">
	<div class="grid-x">
		<div class="cell small-12 app__centrado">
			<h3 class="contacto__subtitulo">{{ __('contacto.entonces')}}</h3>
		</div>
	</div>
	<div class="grid-x grid-padding-x contacto__campos" v-if="!enviando && !error && !enviado">
		<div class="cell small-10 small-offset-1 medium-4 medium-offset-0 large-3 large-offset-2 contacto__col">
			<div class="contacto__popup" v-if="validos.nombre === 1">
				{{__('contacto.error_nombre')}}
			</div>
			<input type="text" v-model="nombre" placeholder="{{ __('contacto.deja_nombre')}}" class="campo" @blur="validar">
		</div>
		<div class="cell small-10 small-offset-1 medium-4 medium-offset-0 large-3 contacto__col">
			<div class="contacto__popup" v-if="validos.correoe === 1">
				{{__('contacto.error_correo')}}
			</div>
			<input type="text" v-model="correoe" placeholder="{{ __('contacto.deja_correo')}}" class="campo" @blur="validar">
		</div>
		<div class="cell small-10 small-offset-1 medium-4 medium-offset-0 large-2 contacto__col">
			<button class="boton boton--revert boton--ancho" @click="enviar">{{ __('contacto.contactame')}}</button>

		</div>
	</div>
	<contacto-enviando v-if="enviando"></contacto-enviando>
	<contacto-enviado v-if="enviado"></contacto-enviado>
	<contacto-error v-if="error"></contacto-error>
</section>