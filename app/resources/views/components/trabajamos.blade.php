<section class="seccion trabajamos grid-container" id="trabajamos">
	<div class="seccion__contenedor">
		<div class="grid-x trabajamos__fila">
			<div class="cell small-10 small-offset-1 medium-4 medium-offset-0 app__centrado trabajamos__elemento">
				<img class="trabajamos__img" src="media/icono-1.png">
				<div class="app__texto">{{ __('trabajamos.somos_compania')}}</div>
			</div>
			<div class="cell small-10 small-offset-1 medium-4 medium-offset-0 app__centrado trabajamos__elemento">
				<img class="trabajamos__img" src="media/icono-2.png">
				<div class="app__texto">{{ __('trabajamos.junto_clientes') }}</div>
			</div>
			<div class="cell small-10 small-offset-1 medium-4 medium-offset-0 app__centrado trabajamos__elemento">
				<img class="trabajamos__img" src="{{ asset('media/icono-3.png') }}">
				<div class="app__texto">{{ __('trabajamos.automatizamos_optimizamos')}}</div>
			</div>
		</div>
		<div class="trabajamos__comenzamos app__centrado">
			{{ __('trabajamos.comenzamos_juntos')}}
		</div>
		<div class="grid-x trabajamos__fila">
			<div class="trabajamos__interesa trabajamos__interesa small-10 small-offset-1 medium-6 medium-offset-3 large-4 large-offset-4">
				<a href="{{ route('contactar', app()->getLocale()) }}" class="boton boton--ancho" data-role="me-interesa">{{ __('trabajamos.me_interesa')}}</a>
			</div>
		</div>
	</div>
</section>