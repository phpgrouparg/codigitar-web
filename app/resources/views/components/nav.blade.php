@section('elementos')
	@foreach($elements as $name => $title)
	  <a href="{{ route('home', app()->getLocale()) . '#' .$name }}" class="nav__menu__elemento ancla"
	  	v-bind:class="{ active: actived === '{{$name}}' }" v-on:click="smoothScroll">{{ __('nav.' . $name )}}</a>
	@endforeach
	  <a href="{{ route('contactar', app()->getLocale()) }}" class="nav__menu__elemento ancla"
	  	v-bind:class="{ active: actived === 'contacto' }" v-on:click="smoothScroll">{{ __('nav.contacto')}}</a>
@endsection

@section('idiomas')
	  <div class="nav__menu__elemento" @click="expandColaps" data-colapsable="idioma_expandido" style="margin-left:0">
	  	<img
        src="{{ asset('/media/bandera-' . app()->getLocale() . '.svg')}}"
        onerror="this.src='{{ asset('media/bandera-' . app()->getLocale() . '.png') }}'"
        title="{{ __('nav.idioma')}}"
        class="nav__menu__bandera"
      />
      <div class="nav__colapsador__caret" :class="{girado: !idioma_expandido, oculto: !montado}" @click="expandColaps" data-colapsable="idioma_expandido"></div>
	  </div>
	  <div class="nav__colapsador__contenido" :class="{expandido: idioma_expandido}">
  		<div v-for="lang in langs" class="nav__colapsador__elemento">
  			<a :href="cambiarIdioma(lang.codigo)" class="nav__menu__elemento">
  			  <img
            :src="params.path + '/bandera-' + lang.codigo + '.svg'"
            v-on:error="changeToPNG"
            class="nav__menu__bandera"
          />&nbsp;<% lang.etiqueta %>
  			</a>
  		</div>
	  </div>
@endsection

  <div class="nav">
    <input type="hidden" ref="params" value='{"path": "{{ URL::asset('media/')}}" }' />
  	<div class="nav__logo">
  		<img class="nav__logo__icono" src="{{ asset('media/logo-blanco-icono.svg') }}" onerror="this.src='{{ asset('media/logo-blanco-icono.png') }}'">
  		<img class="nav__logo__letras" src="{{ asset('media/logo-blanco-letras.svg') }}" onerror="this.src='{{ asset('media/logo-blanco-letras.png') }}'">
  	</div>
		<nav class="nav__menu" role="navigation" v-if="visible">
		  @yield('elementos')
		  <div class="nav__colapsador desplegable">
		  	@yield('idiomas')
		  </div>
		</nav>
		<Slide right class="oculto">
		  @yield('elementos')
		  <div class="nav__colapsador">
				@yield('idiomas')
		  </div>
    </Slide>
  </div>