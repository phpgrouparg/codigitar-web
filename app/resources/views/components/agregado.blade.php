<section class="seccion seccion--revert agregado" id="agregado">
	<div class="seccion__contenedor">
		<div>
			<div class="grid-x">
				<div class="cell small-10 small-offset-1  medium-7 medium-offset-2 large-5 agregado__titulo">
					{{ __('agregado.nuestro_agregado')}}
				</div>
			</div>
			<div class="grid-x">
				<div class="cell small-10 small-offset-1  medium-7 medium-offset-2 large-5 agregado__parrafo">
					{{ __('agregado.forma_progresiva')}}
				</div>
			</div>
		</div>
		<div class="grid-x">
			<div class="cell small-10 small-offset-1  medium-7 medium-offset-2 large-4 agregado__conozca">
				<a href="{{ route('procesos', app()->getLocale()) }}" class="boton boton--transp agregado__conozca__boton" data-role="conozca">
					{{__('agregado.conozca_proceso')}}
					<img class="agregado__conozca__boton__chevron" src="{{ asset('media/chevron.svg') }}" onerror="this.src='{{ asset('media/chevron.png') }}'">
				</a>
			</div>
		</div>
		<div>&nbsp;</div>
	</div>
	<div class="grid-x proximo proximo--blanco">
		<div class="cell small-10 small-offset-1 medium-6 medium-offset-3">
			<button class="proximo__encabezado" data-hacia="clientes">
				{{ __('agregado.ellos_cuentan')}}
			</button>
		</div>
	</div>
</section>