<section class="seccion seccion--revert pie" id="pie">
	<div class="grid-y grid-margin-y pie-wrap">
		<div class="cell small-4 grid-x grid-margin-x pie__arriba">
			<div class="cell small-10 small-offset-1 medium-6 medium-offset-3 large-4 large-offset-4 app__centrado">
				<img src="{{ asset('/media/logo-blanco.svg')}}" class="pie__arriba__logo">
			</div>
		</div>
		<div class="cell small-3 grid-x grid-margin-x">
			<div class="cell small-10 small-offset-1 medium-6 medium-offset-3 large-4 large-offset-4 pie__redes">
				<a href="https://www.linkedin.com/" class="pie__social"><img src="{{ asset('/media/linkedin.png')}}"></a>
				<a href="https://www.instagram.com/" class="pie__social"><img src="{{ asset('/media/instagram.png')}}"></a>
				<a href="https://www.facebook.com/" class="pie__social"><img src="{{ asset('/media/facebook.png')}}"></a>
				<span class="pie__carta">
					<img src="{{ asset('/media/sobre.png') }}" class="icono">
					<img src="{{ asset('/media/correo-team-blanco.png')}}" class="popover">
				</span>
			</div>
		</div>
		<div class="cell small-2 grid-x grid-margin-x pie__abajo">
			<div class="cell small-10 small-offset-1 medium-6 medium-offset-3 large-4 large-offset-4 app__centrado">
				&copy; 2019 CODIGITAR / {{__('pie.derechos_reservados')}}
				/
				<a href="" class="pie__abajo__ancla">{{__('pie.terminos_condiciones')}}</a>
			</div>
		</div>
	</div>
</section>
