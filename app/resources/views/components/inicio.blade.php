<section class="inicio" id="inicio">
	<div class="grid-x inicio__buscamos">
		<div class="cell small-10 small-offset-1 medium-8 medium-offset-2">
			<div class="inicio__texto">{{ __('inicio.buscamos_digitalizacion') }}</div>
		</div>
	</div>
	<div class="inicio__cuentame">
		<button class="boton boton--transp" data-role="cuentame-mas">{{ __('inicio.cuentame_mas')}}</button>
	</div>
			<div class="inicio__vapor"></div>
	<div class="grid-x proximo">
		<div class="cell small-10 small-offset-1 medium-6 medium-offset-3">
			<button class="proximo__encabezado">
				{{ __('inicio.asi_trabajamos') }}
			</button>
		</div>
	</div>
</section>