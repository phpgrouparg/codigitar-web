<section class="seccion seccion--fit clientes grid-container" id="clientes">
	<div class="grid-x grid-margin-x grid-margin-y clientes__grid">
		<div class="cell small-6 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_scsj.png') }}">
		</div>
		<div class="cell small-4 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_lastenia.png') }}">
		</div>
		<div class="cell small-4 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_cluster.png') }}">
		</div>
		<div class="cell small-4 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_yamana.png') }}">
		</div>
		<div class="cell small-4 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_excellence.png') }}">
		</div>
		<div class="cell small-4 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_u24.png') }}">
		</div>
		<div class="cell small-4 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_servimax.png') }}">
		</div>
		<div class="cell small-4 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_setecel.png') }}">
		</div>
		<div class="cell small-4 medium-4 large-2 app__centrado">
			<img class="clientes__img" src="{{ asset('media/logo_digiserv.png') }}">
		</div>
	</div>

</section>