@include('components.contacto-partes')

<section class="seccion seccion--revert seccion--fit contacto contacto--largo" id="contacto" data-forma="largo">
	<div class="grid-x">
		<div class="cell small-12 tablet-6 tablet-offset-1 encabezado">
			{{__('contacto.acordemos_cafe')}}
		</div>
	</div>
	<div class="grid-x grid-margin-x">
		<div class="cell small-12 tablet-6 tablet-offset-1 parrafo">
			{{__('contacto.entendernos_formulario')}}
		</div>
	</div>
	<div class="grid-x grid-margin-x">
		<div class="cell small-12 tablet-6 tablet-offset-1">
			<div v-if="!enviando && !enviado && !error">
				<div class="fila">
					<div class="contacto__popup" v-if="validos.nombre === 1">
						{{__('contacto.error_nombre')}}
					</div>
					<input type="text" v-model="nombre" placeholder="{{__('contacto.deja_nombre')}}" class="campo" @blur="validar">
				</div>
				<div class="fila">
					<div class="contacto__popup" v-if="validos.correoe === 1">
						{{__('contacto.error_correo')}}
					</div>
					<input type="text" v-model="correoe" placeholder="{{__('contacto.deja_correo')}}" class="campo" @blur="validar">
				</div>
        @if(isset($tieneProductos) && $tieneProductos)
          <div class="fila parrafo">
            {{ __('contacto.elegir_productos') }}
          </div>
          <div class="fila grid-x">
            @foreach($productos as $producto)
              <div class="cell small-6 medium-4 tablet-6 large-4 contacto__productos">
                <label class="contacto__productos__label">
                  <input type="checkbox" class="contacto__productos__input" v-model="productos" value="{{ $producto['nombre']}}">
                  <span class="tildado">&#10003;</span>
                  <span class="contacto__productos__caja">
                  </span>
                  {{ $producto['nombre'] }}
                </label>
              </div>
            @endforeach
          </div>
          <div class="contacto__popup" v-if="validos.descripcion === 1">
            {{__('contacto.error_descripcion')}}
          </div>
          <textarea v-model="descripcion" placeholder="{{__('contacto.deja_descripcion')}}" class="campo area" @blur="validar"></textarea>
        @endif
				<div class="grid-x fila">
					<div class="small-4">
						<div class="contacto__popup" v-if="validos.codigo === 1">
							{{__('contacto.error_area')}}
						</div>
						<input type="text" v-model="codarea" placeholder="{{ __('contacto.cod_area')}}" class="campo" @blur="validar">
					</div>
					<div class="contacto--largo__telefono">
						<div class="contacto__popup" v-if="validos.codigo === 1">
							{{__('contacto.error_telefono')}}
						</div>
						<input type="text" v-model="telefono" placeholder="{{__('contacto.telefono')}}" class="campo" @blur="validar">
					</div>
				</div>
				<div class="grid-x fila">
					<div class="small-12 medium-4 medium-offset-8">
						<button class="boton boton--revert boton--ancho" @click="enviar">{{__('contacto.enviar')}}</button>
					</div>
				</div>
			</div>


			<contacto-enviando v-if="enviando"></contacto-enviando>
			<contacto-enviado v-if="enviado"></contacto-enviado>
			<contacto-error v-if="error"></contacto-error>
		</div>
		<div class="cell small-12 medium-5 datos">
			<table class="datos__lista">
				<tbody>
					<tr class="fila fila--dato">
						<td class="icono">
							<img src="{{ asset('media/ubicacion.png') }}">
						</td>
						<td class="texto">
							Av. Córdoba este 669. Loc 1. Capital. San Juan. CP: 5400. Argentina
						</td>
					</tr>
{{-- 					<tr class="fila fila--dato">
						<td class="icono">
							<img src="{{ asset('media/telefono.png') }}">
						</td>
						<td class="texto">
							...
						</td>
					</tr> --}}
					<tr class="fila fila--dato">
						<td class="icono">
							<img src="{{ asset('media/sobre-blanco.png') }}">
						</td>
						<td class="texto">
							<img src="{{ asset('/media/correo-team-blanco.png') }}">
						</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>
</section>