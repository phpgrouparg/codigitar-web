<section class="seccion productos grid-container" id="productos">
  <div class="seccion__contenedor">
    <div class="grid-x grid-margin-x productos__fila">
      @foreach($productos as $producto)
        @php
          switch($loop->count % 3) {
            case 0:
              $num_med = 4;
              break;
            case 1:
              $num_med = ($loop->index < $loop->count - 4) ? 4 : 6;
              break;
            case 2:
              $num_med = ($loop->index < $loop->count - 2) ? 4 : 6;
              break;
          }
        @endphp
        <div class="cell small-10 small-offset-1 medium-{{ $num_med }} medium-offset-0 app__centrado productos__elemento">
          <div class="productos__cuadrado">
            <img class="productos__cuadrado__img"
              src="{{ asset('media/' . $producto['img'] . '.svg') }}"
              onerror="this.src='{{ asset('media/' . $producto['img'] . '.png') }}'">
          </div>
          <h2 class="productos__nombre">{{ $producto['nombre'] }}</h2>
          <h3 class="productos__proposito">{{ $producto['objetivo'] }}</h3>
          <div class="app__texto">{!! $producto['descripcion'] !!}</div>
        </div>
      @endforeach
      </div>
    </div>
  </div>
</section>