<section class="seccion seccion--revert proceso" id="agregado">
	<div class="seccion__contenedor">
		<carousel :items="1">
			<div class="paso">
				<div class="grid-x">
					<div class="small-1 paso__num">1</div>
					<div class="small-10 tablet-5 paso__desc">
						<h2 class="paso__desc__titulo">{{__('proceso.planificacion')}}</h2>
						<p class="paso__desc__texto">
							{{ __('proceso.realiza_relevamiento')}}
						</p>
					</div>
					<div class="show-for-tablet tablet-5 tablet-offset-1 paso__grafico">
						<img src="{{ asset('/media/proceso-1.png')}}">
					</div>
				</div>
			</div>
			<div class="paso">
				<div class="grid-x">
					<div class="small-2 tablet-1 paso__num">2</div>
					<div class="small-10 tablet-5 paso__desc">
						<h2 class="paso__desc__titulo">{{ __('proceso.estimacion')}}</h2>
						<p class="paso__desc__texto">
							{{ __('proceso.nuestro_equipo')}}
						</p>
					</div>
					<div class="show-for-tablet tablet-5 tablet-offset-1 paso__grafico">
						<img src="{{ asset('/media/proceso-2.png')}}">
					</div>
				</div>
			</div>
			<div class="paso">
				<div class="grid-x">
					<div class="small-2 tablet-1 paso__num">3</div>
					<div class="small-10 tablet-5 paso__desc">
						<h2 class="paso__desc__titulo">{{__('proceso.desarrollo')}}</h2>
						<p class="paso__desc__texto">
							{{ __('proceso.codigitar_lleva')}}
						</p>
					</div>
					<div class="show-for-tablet tablet-5 tablet-offset-1 paso__grafico">
						<img src="{{ asset('/media/proceso-3.png')}}">
					</div>
				</div>
			</div>
			<div class="paso">
				<div class="grid-x">
					<div class="small-10 tablet-5 paso__desc">
						<h2 class="paso__desc__titulo">{{__('proceso.nuestro_valor')}}</h2>
						<p class="paso__desc__texto">
							{{ __('proceso.forma_progresiva')}}
						</p>
					</div>
					<div class="show-for-tablet tablet-5 tablet-offset-1 paso__grafico">
						<img src="{{ asset('/media/proceso-4.png')}}">
					</div>
				</div>
			</div>
			<div class="paso">
				<div class="grid-x">
					<div class="small-2 tablet-1 paso__num">4</div>
					<div class="small-10 tablet-5 paso__desc">
						<h2 class="paso__desc__titulo">{{ __('proceso.implementacion')}}</h2>
						<p class="paso__desc__texto">
							{{ __('proceso.implementamos_producto')}}
						</p>
					</div>
					<div class="show-for-tablet tablet-5 tablet-offset-1 paso__grafico">
						<img src="{{ asset('/media/proceso-5.png')}}">
					</div>
				</div>
			</div>
			<div class="paso">
				<div class="grid-x">
					<div class="small-2 tablet-1 paso__num">5</div>
					<div class="small-10 tablet-5 paso__desc">
						<h2 class="paso__desc__titulo">{{ __('proceso.capacitacion')}}</h2>
						<p class="paso__desc__texto">
							{{__('proceso.adaptamos_tecnologia')}}
						</p>
					</div>
					<div class="show-for-tablet tablet-5 tablet-offset-1 paso__grafico">
						<img src="{{ asset('/media/proceso-6.png')}}">
					</div>
				</div>
			</div>
			<div class="paso">
				<div class="grid-x">

					<div class="small-10 small-offset-1 tablet-8 tablet-offset-2 paso__desc">
						<p class="paso__desc__texto app__centrado">
							{{ __('proceso.conoces_proceso')}}
							<br>
							{{ __('proceso.te_contamos')}} <strong>{{ __('proceso.valor_agregado')}}</strong>.
						</p>
						<h3 class="paso__desc__subtitulo app__centrado">{{__('proceso.emprendamos_juntos')}}</h3>

					</div>
				</div>
				<div class="paso__centro">
					<a href="/contactar" class="boton boton--revert">{{__('proceso.de_acuerdo')}}</a>
				</div>
			</div>
		</carousel>
		<div class="proceso__prev" @click="previo" id="proceso-previo">
			<img class="proceso__flecha" src="{{ asset('media/chevronl.svg') }}" onerror="this.src='{{ asset('media/chevronl.png') }}'">
		</div>
		<div class="proceso__post" @click="posterior" id="proceso-posterior">
			<img class="proceso__flecha" src="{{ asset('media/chevron.svg') }}" onerror="this.src='{{ asset('media/chevron.png') }}'">
		</div>
	</div>
	<div class="grid-x proximo proximo--blanco">
		<div class="cell small-10 small-offset-1 medium-6 medium-offset-3">
			<button class="proximo__encabezado" data-hacia="clientes">
				{{__('proceso.ellos_cuentan')}}
			</button>
		</div>
	</div>
</section>