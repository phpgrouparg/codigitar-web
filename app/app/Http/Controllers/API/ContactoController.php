<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Mail;

class HomeController extends Controller
{
	public function contactoMinimo(Request $request)
	{
		$request->validate([
			'nombre' => 'required|string|min:3',
			'correoe' => 'required|email',
      'productos' => 'nullable|array',
      'productos.*' => 'required|string',
      'description' => 'nullable|string'
		], [
			'nombre' => 'Nombre no fue definido',
			'correoe' => 'Dirección de correo inválida'
		]);
		// Mail::to('desdeweb@codigitar.com')->subject('Usuario pide comunicación desde inicio')->body('Nombre: ' . $request->nombre . '; correoe:' . $request->correoe);
		// if(Mail::failures()) {
  // 		return response()->json(['error' => 'mail-not-sent'], 500);
  // 	}
    $contenido = 'Nombre: ' . $request->nombre . '; correoe:' . $request->correoe;
    if($request->descripcion) {
      $contenido .= '\n' . $request->descripcion;
    }
    if($request->productos && count($request->productos)) {
      $contenido .= '\nEstá interesado en:';
      foreach($request->productos as $producto) {
        $contenido .= '\n- ' . $producto;
      }
    }
  	$sent = mail('desdeweb@codigitar.com', 'Usuario pide comunicación desde página de inicio', $contenido);
  	if($sent) {
  		return response()->json(['resultado' => 'enviado']);
  	}
  	return response()->json(['error' => 'mail-not-sent'], 500);
	}
}