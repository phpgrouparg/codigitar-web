<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Config;

class HomeController extends Controller
{

  private $traducciones = [];
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    //$this->middleware('auth');
    $this->elements = [
      'inicio' => 'Inicio',
      'trabajamos' => '¿Como trabajamos?',
      'agregado' => 'Proceso Codigitar',
      'podemos' => 'Lo que hacemos',
      'codigitadores' => 'Codigitadores'
    ];
    /* La traducción de rutas no permite hacerlas caché
    * TODO: borrar o reactivar lo siguiente (y completarlo) según se decida
    */
    // foreach(array_keys(config('app.available_locales')) as $lang) {
    //   $this->traducciones[$lang] = [];
    //   foreach(config('app.enabled_routes') as $ruta) {
    //     $this->traducciones[$lang][$ruta] = __('routes.' . $ruta);
    //   }
    // }
  }

  private function baseView($locale) {
    \App::setLocale($locale);
    return [
      'elements' => $this->elements,
      'locale' => $locale,
    ];
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index($locale = 'en')
  {

    $imgs = ['omar_correo' => 'media/correo-omar.PNG', 'nestor_correo' => 'media/correo-nestor.PNG', 'patricio_correo' => 'media/correo-patricio.PNG'];
    foreach($imgs as $k => $ruta) {
      $imgs[$k] = getimagesize(public_path($ruta));
    }
    $tecnos = [];
    if ($handle = opendir(public_path('media/tecnologias'))) {
      while (false !== ($entry = readdir($handle))) {
        $elm = explode('.', $entry);
        if (count($elm) > 1 && $elm[count($elm) - 1] === 'svg') {
          array_push($tecnos, $elm[0]);
        }
      }
      closedir($handle);
    }

    return view('home', array_merge($this->baseView($locale), [
      'imgs' => $imgs,
      'build' => 'app',
      'tecnos' => $tecnos
    ]));
  }

  public function redireccionar(Request $request)
  {
    $arg1 = $request->segment(1);
    if(strlen($arg1) == 2 && in_array($arg1, config('app.available_locales'))) {
      return redirect(app()->getLocale());

    }
    if(!$arg1) {
      if (Session::has('locale')) {
        app()->setLocale(Session::get('locale'));
      } else {
        // detección de navegador, de momento bloqueada
        // if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        //   $languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        // } else {
          $languages[0] = config('app.fallback_locale');
        // }
        if (in_array($languages[0], array_keys(config('app.available_locales')))) app()->setLocale($languages[0]);
      }
      $page = '';
    }

    $page = $arg1 ? $arg1 : '';

    $route = (in_array($page, config('app.enabled_routes'))) ? '/' . $page : '';
    return redirect(app()->getLocale() . $route);
  }

  public function contactar($locale = 'en')
  {
    \App::setLocale($locale);
    return view('contactar', array_merge($this->baseView($locale), [
      'build' => 'contactar'
    ]));
  }

  public function procesos($locale = 'en')
  {
    \App::setLocale($locale);
    return view('procesos', array_merge($this->baseView($locale), [
      'build' => 'procesos'
    ]));
  }

  public function productos($locale = 'en')
  {
    \App::setLocale($locale);
    return view('productos', array_merge($this->baseView($locale), [
      'tieneProductos' => true,
      'productos' => [
        ['nombre'=> 'ComandoApp', 'objetivo' => __('productos.gestion_gimnasios'), 'descripcion' => __('productos.enfocado_retencion'), 'img' => 'comandoapp'],
        ['nombre'=> 'Cuiser', 'objetivo' => __('productos.encuestas'), 'descripcion' => __('productos.soporte_encuestas'), 'img' => 'cuiser'],
        ['nombre'=> 'Gestcolar', 'objetivo' => __('productos.gestion_escuela'), 'descripcion' => __('productos.soporte_cobro'), 'img' => 'gestcolar'],
        ['nombre'=> 'Optimus', 'objetivo' => __('productos.gestion_inventarios'), 'descripcion' => __('productos.soporte_inventarios'), 'img' => 'optimus'],
        ['nombre'=> 'Vastum', 'objetivo' => __('productos.gestion_residuos'), 'descripcion' => __('productos.soporte_trazabilidad'), 'img' => 'vastum'],
        ['nombre'=> 'Gort', 'objetivo' => __('productos.ordenes_trabajo'), 'descripcion' => __('productos.soporte_trazabilidad'), 'img' => 'gort']
      ],
      'build' => 'productos'
    ]));
  }
}
