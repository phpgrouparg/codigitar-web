const mix = require('laravel-mix');
require('laravel-mix-polyfill');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
['app', 'contactar', 'procesos', 'productos'].forEach(function (elm) {
  mix.js('resources/js/' + elm + '.js', 'public/js')
    .webpackConfig({
      resolve: {
         alias: {
           '@': path.resolve('resources/sass')
         }
       }
    })
    .polyfill({
      enabled: true,
      useBuiltIns: "usage",
      targets: {"firefox": "50", "ie": 10}
    })
    .sass('resources/sass/' + elm + '.scss', 'public/css')
    .options({
      postCss: [
        require('postcss-css-variables')()
      ]
     })
    .sourceMaps();
});